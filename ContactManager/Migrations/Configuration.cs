namespace ContactManager.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ContactManager.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactManager.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ContactManager.Models.ApplicationDbContext context)
        {

            context.Benefits.AddOrUpdate
                (
                p => p.Name, 
                
                new Benefits
                {
                Name= "Name1",
                Address= "Adress1",
                Category= "Food",
                Benefit= "15% on all"
                },

                new Benefits
                {
                Name= "Name2",
                Address= "Adress2",
                Category= "Entertainment",
                Benefit= "10% on all"
                }
                
                );
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
